/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{ProcessingFatal, ProcessingIgnore, ProcessingSuccess}
import org.apache.logging.log4j.scala.Logging

import java.io.File

/** Handles the deletion of binaries
  */
trait BinaryHandler {
  self: Logging & AppSettings =>

  def deleteBinaries(
      processor: ReportProcessor,
      binaries: List[String]
  ): Unit = {
    if (binaries.nonEmpty) {
      logger.debug(s"Try to delete ${binaries.size} binaries")
      val result = binaries
        .filter(_.startsWith("file://"))
        .map(_.replaceFirst("file://", ""))
        .map(fileName => (new File(fileName), fileName))
        .map(file =>
          (if (processor.dryRun) file._1.exists else file._1.delete, file._2)
        )
        .foldLeft[List[String]](List())((errors, x) =>
          if (!x._1) {
            logger.warn(
              s"Deletion of file ${x._2} failed${if (processor.dryRun) { " (dry-run mode)" }
              else { "" }}!"
            )
            errors :+ s"${x._2} deletion failed"
          } else {
            errors
          }
        )
      if (result.isEmpty) {
        logger.info(
          s"Deletion of binaries for record ${processor.id} successful${if (processor.dryRun) { " (dry-run mode)" }
          else { "" }}"
        )
        processor.addReport(
          ProcessingSuccess,
          "Binaries successfully deleted or ignored in case of remote binaries"
        )
      } else {
        logger.warn(
          s"Deletion of binaries for record ${processor.id} failed${if (processor.dryRun) { " (dry-run mode)" }
          else { "" }}"
        )
        processor.addReport(ProcessingFatal, result.mkString("; "))
      }
    } else {
      processor.addReport(ProcessingIgnore, "No attached local binaries")
    }
  }

  def deleteCachedBinaries(
      processor: ReportProcessor,
      binaries: List[String]
  ): Unit = {
    if (binaries.nonEmpty) {
      logger.debug(s"Try to delete ${binaries.size} cached thumbnails")
      val result = binaries
        .map(_.replaceFirst("file://", ""))
        .map(_.replaceFirst(mediaFolderRootPath, mediaFolderCacheRootPath))
        .map(fileName => (new File(fileName), fileName))
        .map(file =>
          (if (processor.dryRun) file._1.exists else file._1.delete, file._2)
        )
        .foldLeft[List[String]](List())((errors, x) =>
          if (!x._1) {
            logger.info(
              s"No thumbnail ${x._2} exists${if (processor.dryRun) { " (dry-run mode)" }
              else { "" }}!"
            )
            errors :+ s"Deletion of thumbnail ${x._2} failed (probably because there are no cached thumbnails...)"
          } else {
            errors
          }
        )
      if (result.isEmpty) {
        logger.info(
          s"Deletion of cached thumbnails for record ${processor.id} successful${if (processor.dryRun) { " (dry-run mode)" }
          else { "" }}"
        )
        processor.addReport(
          ProcessingSuccess,
          "Cached thumbnails deletion successful"
        )
      } else {
        logger.info(
          s"Thumbnail ${processor.id} not available${if (processor.dryRun) { " (dry-run mode)" }
          else { "" }}"
        )
        processor.addReport(ProcessingIgnore, result.mkString("; "))
      }
    } else {
      processor.addReport(ProcessingIgnore, "No attached local binaries")
    }
  }
}
