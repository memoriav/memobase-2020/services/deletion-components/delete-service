package ch.memobase

import scala.util.Try
import java.security.cert.CertificateFactory
import java.security.KeyStore
import org.apache.http.ssl.SSLContexts
import java.io.FileInputStream
import javax.net.ssl.SSLContext
import java.io.File

object TlsUtils {

  /** Build the required SSL/TLS context to check server certificates
    *
    * @param caCertPath
    *   Path to CA certificate
    * @return
    */
  def buildSSLContext(caCertPath: String): Try[SSLContext] = {
    Try {
      val factory = CertificateFactory.getInstance("X.509")
      val trustedCa =
        factory.generateCertificate(new FileInputStream(new File(caCertPath)))
      val trustStore = KeyStore.getInstance("pkcs12")
      trustStore.load(null, null)
      trustStore.setCertificateEntry("ca", trustedCa)
      SSLContexts.custom.loadTrustMaterial(trustStore, null).build
    }
  }
}
