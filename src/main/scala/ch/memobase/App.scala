/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.logging.log4j.scala.Logging

object App
    extends scala.App
    with Logging
    with AppSettings
    with PostgresClient
    with MariadbClient
    with KafkaMessageDeleter
    with BinaryHandler {

  val esClient =
    MemobaseElasticClient(esApiKeyId, esApiKeySecret, esCaCertPath, esHosts).get

  private def deleteDocument(deleteObject: DeleteObject): Unit = {
    val processedRecord = new ReportProcessor(deleteObject, dryRun)
    if (dryRun) {
      esClient.existsByIdInESIndices(processedRecord, frontendDocumentsIndex)
      esClient.existsByIdInESIndices(
        processedRecord,
        apiDocumentsIndex,
        "https://memobase.ch/record/"
      )
    } else {
      esClient.deleteByIdInESIndices(processedRecord, frontendDocumentsIndex)
      esClient.deleteByIdInESIndices(
        processedRecord,
        apiDocumentsIndex,
        "https://memobase.ch/record/"
      )
    }
    val binaryUrls = deleteMariaDBEntry(processedRecord)
    deleteBinaries(processedRecord, binaryUrls)
    deleteCachedBinaries(processedRecord, binaryUrls)
    deleteMessage(processedRecord, producer)
    processedRecord.sendReports(
      sessionId,
      reportStepId,
      reportingTopic,
      producer
    )
  }

  lazy val producer = new KafkaProducer[String, String](producerProps)
  for (deleteObject <- ids) {
    logger.info(
      s"Processing ${deleteObject.name.toLowerCase} ${deleteObject.id}"
    )
    val records = getRecords(deleteObject)
    val hasRecordsInSSoT = records.nonEmpty
    if (hasRecordsInSSoT) {
      logger.debug(s"${records.size} documents to be deleted")
    } else {
      logger.info(s"0 documents in SSoT found.")
    }

    deleteObject match {
      case Document(_) if hasRecordsInSSoT =>
        deleteDocument(deleteObject)

      case DocumentWithOriginalId(_, _) if hasRecordsInSSoT =>
        for (documentId <- records) {
          val documentDeleteObject = Document(documentId)
          deleteDocument(documentDeleteObject)
        }

      case Document(_) | DocumentWithOriginalId(_, _) =>
        logger.info("Finishing")

      case RecordSet(recordSetId) =>
        if (recursiveDelete) {
          val processedRecordSet = new ReportProcessor(deleteObject, dryRun)
          if (dryRun) {
            esClient.existsByIdInESIndices(
              processedRecordSet,
              frontendRecordSetsIndex
            )
            esClient.existsByIdInESIndices(
              processedRecordSet,
              apiRecordSetsIndex
            )
          } else {
            esClient.deleteByIdInESIndices(
              processedRecordSet,
              frontendRecordSetsIndex
            )
            esClient.deleteByIdInESIndices(
              processedRecordSet,
              apiRecordSetsIndex
            )
          }
          processedRecordSet.sendReports(
            sessionId,
            reportStepId,
            reportingTopic,
            producer
          )
        }
        if (hasRecordsInSSoT) {
          val processedRecords = records.flatMap(recId => {
            DeleteObject.mapToDeleteType(recId) match {
              case Some(dO) => Some(new ReportProcessor(dO, dryRun))
              case None     => None
            }
          })
          // Don't do lookups for documents if in dry run mode
          if (!dryRun) {
            esClient.deleteByQueryInESIndices(
              processedRecords,
              frontendDocumentsIndex,
              "recordSet.facet",
              recordSetId
            )
            esClient.deleteByQueryInESIndices(
              processedRecords,
              apiDocumentsIndex,
              "isOrWasPartOf.keyword",
              s"mbrs:$recordSetId"
            )
            for (processedRecord <- processedRecords) {
              val binaryUrls = deleteMariaDBEntry(processedRecord)
              deleteBinaries(processedRecord, binaryUrls)
              deleteCachedBinaries(processedRecord, binaryUrls)
              deleteMessage(processedRecord, producer)
              processedRecord.sendReports(
                sessionId,
                reportStepId,
                reportingTopic,
                producer
              )
            }
          }
        }
      case Institution(institutionId) =>
        if (recursiveDelete) {
          val processedInstitution = new ReportProcessor(deleteObject, dryRun)
          // Don't do lookups for recordSets if in dry run mode
          if (processedInstitution.dryRun) {
            esClient.existsByIdInESIndices(
              processedInstitution,
              frontendInstitutionsIndex
            )
            esClient.existsByIdInESIndices(
              processedInstitution,
              apiInstitutionsIndex,
              "https://memobase.ch/institution/"
            )
          } else {
            val processedRecordSets = records
              .map(_.split('-').take(2).mkString("-"))
              .toSet
              .flatMap((recordSetId: String) => {
                DeleteObject.mapToDeleteType(recordSetId) match {
                  case Some(dO) => Some(new ReportProcessor(dO, dryRun))
                  case None     => None
                }
              })
              .toList
            esClient.deleteByIdInESIndices(
              processedInstitution,
              frontendInstitutionsIndex
            )
            esClient.deleteByIdInESIndices(
              processedInstitution,
              apiInstitutionsIndex,
              "https://memobase.ch/institution/"
            )
            esClient.deleteByQueryInESIndices(
              processedRecordSets,
              frontendRecordSetsIndex,
              "institution.filter",
              institutionId
            )
            esClient.deleteByQueryInESIndices(
              processedRecordSets,
              apiRecordSetsIndex,
              "hasOrHadHolder",
              institutionId
            )
            processedRecordSets.foreach(
              _.sendReports(sessionId, reportStepId, reportingTopic, producer)
            )
          }
          processedInstitution.sendReports(
            sessionId,
            reportStepId,
            reportingTopic,
            producer
          )
        }
        if (hasRecordsInSSoT) {
          val processedRecords = records.flatMap(recId => {
            DeleteObject.mapToDeleteType(recId) match {
              case Some(dO) => Some(new ReportProcessor(dO, dryRun))
              case None     => None
            }
          })
          // Don't do lookups for documents if in dry run mode
          if (!dryRun) {
            esClient.deleteByQueryInESIndices(
              processedRecords,
              frontendDocumentsIndex,
              "institution.filter",
              institutionId
            )
            esClient.deleteByQueryInESIndices(
              processedRecords,
              apiDocumentsIndex,
              "hasOrHadHolder.@id",
              s"mbcb:$institutionId"
            )
            for (processedRecord <- processedRecords) {
              val binaryUrls = deleteMariaDBEntry(processedRecord)
              deleteBinaries(processedRecord, binaryUrls)
              deleteCachedBinaries(processedRecord, binaryUrls)
              deleteMessage(processedRecord, producer)
              processedRecord.sendReports(
                sessionId,
                reportStepId,
                reportingTopic,
                producer
              )
            }
          }
        }
    }

    if (!dryRun && hasRecordsInSSoT) {
      val noOfRowsAffected = markRecordsInPSQLAsDeleted(deleteObject)
      logger.info(s"$noOfRowsAffected documents updated in PostgreSQL")
    }
  }
  logger.debug("Closing connection to Elasticsearch instance")

  esClient.close
  closeMariadbConnection()
  closePostgresConnection()
  logger.debug("Closing Kafka producer")
  producer.flush()
  producer.close()
}
