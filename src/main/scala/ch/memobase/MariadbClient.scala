/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import org.apache.logging.log4j.scala.Logging

import java.sql.{Connection, DriverManager}
import scala.util.{Failure, Success, Try}

trait MariadbClient {
  self: Logging & AppSettings =>

  private type DeletionResult =
    Try[(Option[String], Boolean, Boolean, Boolean, Boolean)]

  private val noRows = Success((None, false, false, false, false))

  private var connection = getConnection

  private def getConnection: Connection = {
    val c =
      DriverManager.getConnection(mariaDBDSN, mariaDBUser, mariaDBPassword)
    c.setAutoCommit(false)
    c
  }

  private def isLocalFile(uri: String): Boolean = uri.startsWith("file:")

  private def generateDeletionResultMessage(
      resourceName: String,
      entitiesTable: Boolean,
      metadataTable: Boolean,
      manifestV2Table: Boolean,
      manifestV3Table: Boolean
  ): String = {
    s"$resourceName entry deleted in MariaDB tables " +
      s"${if (entitiesTable) {
        "entities, "
      } else {
        ""
      }}" +
      s"${if (metadataTable) {
        "metadata, "
      } else {
        ""
      }}" +
      s"${if (manifestV2Table) {
        "iiif_manifests_v2, "
      } else {
        ""
      }}" +
      s"${if (manifestV3Table) {
        "iiif_manifests_v3"
      } else {
        ""
      }}"
  }

  private def updateDigitalObjectResult(
      record: ReportProcessor,
      deletionResult: DeletionResult
  ): Option[String] = {
    deletionResult match {
      case Success((None, false, false, false, false)) =>
        record.addReport(
          ProcessingIgnore,
          s"Digital object not found in media DB"
        )
        None
      case Success((uri, entities, metadata, manifestV2, manifestV3)) =>
        record.addReport(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Digital object",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
        if (uri.isDefined && isLocalFile(uri.get)) {
          Some(uri.get)
        } else { None }
      case Failure(ex) =>
        record.addReport(
          ProcessingFatal,
          s"Digital object media DB entries deletion failed${if (record.dryRun) { " (dry-run mode)" }
          else { "" }}: ${ex.getMessage}"
        )
        None
    }

  }

  // scalastyle:off
  private def updateThumbnailResult(
      record: ReportProcessor,
      posterDeletionResult: DeletionResult,
      snippetDeletionResult: DeletionResult
  ): Option[String] = {
    (posterDeletionResult, snippetDeletionResult) match {
      case (Failure(e), _) =>
        // Something went wrong
        record.addReport(
          ProcessingFatal,
          s"Poster entries deletion in MariaDB failed${if (record.dryRun) { " (dry-run mode)" }
          else { "" }}: ${e.getMessage}"
        )
        None
      case (_, Failure(e)) =>
        // Something went wrong
        record.addReport(
          ProcessingFatal,
          s"Snippet entries deletion in MariaDB failed${if (record.dryRun) { " (dry-run mode)" }
          else { "" }}: ${e.getMessage}"
        )
        None
      case (`noRows`, `noRows`) =>
        record.addReport(
          ProcessingIgnore,
          "No thumbnail entries found in MariaDB"
        )
        None
      case (
            Success((uri, entities, metadata, manifestV2, manifestV3)),
            `noRows`
          ) =>
        record.addReport(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Poster",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
        if (uri.isDefined && isLocalFile(uri.get)) {
          Some(uri.get)
        } else {
          logger.warn("Poster entry does not point to a local file!")
          None
        }
      case (
            `noRows`,
            Success((uri, entities, metadata, manifestV2, manifestV3))
          ) =>
        record.addReport(
          ProcessingSuccess,
          generateDeletionResultMessage(
            "Snippet",
            entities,
            metadata,
            manifestV2,
            manifestV3
          )
        )
        if (uri.isDefined && isLocalFile(uri.get)) {
          Some(uri.get)
        } else {
          logger.warn("Snippet entry does not point to a local file!")
          None
        }
      case (_, _) =>
        record.addReport(
          ProcessingWarning,
          "Snippets and posters entries found in MariaDB. This should not happen!"
        )
        None
    }
  }
  // scalastyle:on

  /** Updates `Processor` instance if digital objects and/or thumbnails were
    * identified in MariaDB table. The following cases are possible:
    *   - Digital object with local binary and thumbnail present
    *   - Digital object with remote binary and thumbnail present
    *   - Only a digital object with local binary present
    *   - Only a digital object with remote binary present
    *   - Only a thumbnail present
    *
    * @param record
    *   `ProcessedDocument` instance
    * @return
    *   Binary url if present, else None
    */
  // noinspection ScalaStyle
  def deleteMariaDBEntry(record: ReportProcessor): List[String] = {
    val digitalObjectId = record.id + "-1"
    val digitalObjectDeletionResult = deleteRows(digitalObjectId)
    val posterDeletionResult = deleteRows(s"$digitalObjectId-poster")
    val introDeletionResult = deleteRows(s"$digitalObjectId-intro")
    (if (record.dryRun) {
       Try(connection.rollback())
     } else {
       Try(connection.commit())
     }) match {
      case Success(_) =>
        logger.info(
          s"MariaDB entries for record ${record.id} successfully deleted${if (record.dryRun) " (dry-run mode)"
          else ""}."
        )
        val digitalObjectBinaryUrl = updateDigitalObjectResult(
          record,
          digitalObjectDeletionResult
        )

        val thumbnailBinaryUrl = updateThumbnailResult(
          record,
          posterDeletionResult,
          introDeletionResult
        )
        // reportDeletedRows(record, digitalObjectDeletionResult)
        List(digitalObjectBinaryUrl, thumbnailBinaryUrl).flatten
      case Failure(ex) =>
        logger.warn(
          s"Deletion of MariaDB entries for record ${record.id} failed${if (record.dryRun) { " (dry-run mode)" }
          else { "" }}: ${ex.getMessage}."
        )
        updateDigitalObjectResult(
          record,
          Failure(ex)
        )

        updateThumbnailResult(
          record,
          Failure(ex),
          Failure(ex)
        )
        List()
    }
  }

  private def checkDBConnection(timeout: Int): Unit = {
    if (connection == null || !connection.isValid(timeout)) {
      logger.info("Connection to MariaDB is non-existent or invalid.")
      if (!connection.isClosed) {
        connection.close()
      }
      connection = getConnection
    }
  }

  def closeMariadbConnection(): Unit = {
    logger.debug("Closing connection to MariaDB database")
    connection.close()
  }

  /** Deletes related rows
    *
    * @param id
    *   Record's id
    * @return
    *   An optional media file URI and success status for each row
    */
  private def deleteRows(
      id: String
  ): DeletionResult =
    Try {
      val stmt = connection.createStatement
      val deleteEntities =
        s"DELETE FROM entities WHERE sig = '$id' RETURNING uri"
      val resultSet = stmt.executeQuery(deleteEntities)
      val (entitiesDeleted, uri) = if (resultSet.next) {
        val u = Try(resultSet.getString("uri")).toOption
        if (u.isEmpty) {
          logger.warn(s"No uri found in table entities for record $id-1")
        }
        (true, u)
      } else {
        (false, None)
      }
      val delete: String => Boolean = tableName => {
        val deleteQuery = s"DELETE FROM $tableName WHERE sig LIKE '$id%'"
        val resultSet = stmt.executeUpdate(deleteQuery)
        resultSet > 0
      }
      (
        uri,
        entitiesDeleted,
        delete("metadata"),
        delete("iiif_manifests_v2"),
        delete("iiif_manifests_v3")
      )
    }
}
