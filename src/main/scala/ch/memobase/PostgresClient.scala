/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{
  DeleteObject,
  Document,
  DocumentWithOriginalId,
  Institution,
  RecordSet
}
import org.apache.logging.log4j.scala.Logging

import java.sql.{Connection, DriverManager}
import java.time

trait PostgresClient {
  self: Logging & AppSettings =>

  private var connection: Connection = getConnection

  private def getConnection: Connection = {
    logger.debug(s"Connecting to $postgresDBHost")
    DriverManager.getConnection(
      postgresDBHost,
      postgresDBUser,
      postgresDBPassword
    )
  }

  private def checkDBConnection(timeout: Int): Unit = {
    if (connection == null || !connection.isValid(timeout)) {
      logger.info("Connection to PostgreSQL DB is non-existent or invalid.")
      if (!connection.isClosed) {
        connection.close()
      }
      connection = getConnection
    }
  }

  def closePostgresConnection(): Unit = {
    logger.debug("Closing connection to PostgresSQL database")
    connection.close()
  }

  def getRecords(deleteType: DeleteObject): Seq[String] = {
    checkDBConnection(1000)
    var res = Seq[String]()
    try {
      val statement = connection.createStatement()
      val resultSet = deleteType match {
        case Document(id) =>
          statement.executeQuery(
            s"SELECT memobase_id FROM memobase_record where memobase_id = '$id' and status != 'DELETED'"
          )
        case DocumentWithOriginalId(documentId, recordSetId) =>
          statement.executeQuery(
            s"SELECT memobase_id FROM memobase_record where original_id = '$documentId' and record_set_id = '$recordSetId' and status != 'DELETED'"
          )
        case RecordSet(id) =>
          statement.executeQuery(
            s"SELECT memobase_id FROM memobase_record where record_set_id = '$id' and status != 'DELETED'"
          )
        case Institution(id) =>
          statement.executeQuery(
            s"SELECT memobase_id FROM memobase_record where institution_id = '$id' and status != 'DELETED'"
          )
      }

      while (resultSet.next()) {
        val recordid = resultSet.getString("memobase_id")
        res = res :+ recordid
      }
    } catch {
      case e: Throwable => e.printStackTrace()
    }
    res
  }

  def markRecordsInPSQLAsDeleted(deleteType: DeleteObject): Int = {
    checkDBConnection(1000)
    val statement = connection.createStatement()
    val timeNow: String = time.LocalDateTime.now().toString
    (deleteType: @annotation.nowarn("msg=match may not be exhaustive")) match {
      case Document(_) =>
        statement.executeUpdate(
          s"UPDATE memobase_record SET status = 'DELETED', modification_time = '$timeNow' WHERE memobase_id = '${deleteType.id}'"
        )
      case RecordSet(_) =>
        statement.executeUpdate(
          s"UPDATE memobase_record SET status = 'DELETED', modification_time = '$timeNow' WHERE record_set_id = '${deleteType.id}'"
        )
      case Institution(_) =>
        statement.executeUpdate(
          s"UPDATE memobase_record SET status = 'DELETED', modification_time = '$timeNow' WHERE institution_id = '${deleteType.id}'"
        )
    }
  }
}
