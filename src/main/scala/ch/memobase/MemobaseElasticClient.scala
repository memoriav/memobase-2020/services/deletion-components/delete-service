/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import com.sksamuel.elastic4s
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.http.{JavaClient, NoOpRequestConfigCallback}
import com.sksamuel.elastic4s.{
  ElasticClient,
  ElasticProperties,
  RequestFailure,
  RequestSuccess
}
import org.apache.logging.log4j.scala.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder

import org.apache.http.message.BasicHeader
import org.apache.http.Header

import java.util.Base64
import java.nio.charset.StandardCharsets
import java.util
import scala.jdk.CollectionConverters._
import scala.util.Try

class MemobaseElasticClient(esClient: ElasticClient) extends Logging {

  def existsByIdInESIndices(
      record: ReportProcessor,
      index: String,
      prefix: String = ""
  ): Unit = {
    logger.debug(
      s"Querying documents in index $index where `_id = $prefix${record.id}`"
    )
    esClient
      .execute {
        search(index).matchQuery("_id", s"$prefix${record.id}")
      }
      .map {
        case res if res.isSuccess && res.status < 400 =>
          logger.debug(
            s"Document $prefix${record.id} exists in Elasticsearch index $index"
          )
          record.addReport(
            ProcessingSuccess,
            s"Document exists in Elasticsearch index $index"
          )
        case res if res.isSuccess =>
          logger.debug(
            s"Document $prefix${record.id} does not exist in Elasticsearch index $index"
          )
          record.addReport(
            ProcessingIgnore,
            s"Document does not exist in Elasticearch index $index"
          )

        case err =>
          logger.warn(
            s"Check of existence for record $prefix${record.id} in Elasticsearch index $index failed: ${err.error.reason}"
          )
          record.addReport(
            ProcessingFatal,
            s"Check for existence for document in Elasticsearch index $index failed: ${err.error.reason}"
          )
      }
      .await
  }

  def deleteByQueryInESIndices(
      records: Seq[ReportProcessor],
      index: String,
      field: String,
      fieldValue: String
  ): Unit = {
    logger.debug(
      s"Deleting documents by query in index $index where `$field = $fieldValue`"
    )
    esClient
      .execute {
        deleteByQuery(index, termQuery(field, fieldValue))
      }
      .map {
        case RequestFailure(status, _, _, error) =>
          records.foreach(rec => {
            logger.warn(
              s"Deletion of document ${rec.id} by query in Elasticsearch index $index failed with status $status: ${error.reason}"
            )
            rec.addReport(
              ProcessingFatal,
              s"Deletion by query in Elasticsearch index $index failed with status $status: ${error.reason}"
            )
          })
        case RequestSuccess(_, _, _, result) =>
          val totalDeleted = result.swap.toOption.map(_.total).getOrElse(0L)
          records.foreach(rec => {
            if (totalDeleted > 0) {
              logger.debug(
                s"Deletion of document ${rec.id} by query in Elasticsearch index $index successful"
              )
              rec.addReport(
                ProcessingSuccess,
                s"Deletion of document by query in Elasticsearch index $index successful"
              )
            } else {
              logger.debug(
                s"${rec.id} not deleted in Elasticsearch index $index as no documents with query could be found"
              )
              rec.addReport(
                ProcessingIgnore,
                s"Not deleted in Elasticsearch index $index as no documents with query could be found"
              )
            }
          })
      }
      .await
  }

  def deleteByIdInESIndices(
      record: ReportProcessor,
      index: String,
      prefix: String = ""
  ): Unit = {
    esClient
      .execute {
        logger.info(
          s"Deleting $prefix${record.id} in index $index"
        )
        deleteById(index, s"$prefix${record.id}")
      }
      .map {
        case success: RequestSuccess[_] if success.status < 400 =>
          logger.debug(
            s"Deletion of record $prefix${record.id} by id in Elasticsearch index $index successful"
          )
          record.addReport(
            ProcessingSuccess,
            s"Record deletion by id in Elasticsearch index $index successful"
          )
        case _: RequestSuccess[_] =>
          logger.debug(
            s"Deletion of record $prefix${record.id} by id in Elasticsearch failed since record does not exist in index $index"
          )
          record.addReport(
            ProcessingIgnore,
            s"Deletion of record $prefix${record.id} by id in Elasticsearch failed since record does not exist in index $index"
          )
        case failure: RequestFailure =>
          logger.warn(
            s"Deletion of record $prefix${record.id} by id failed in Elasticsearch index $index: ${failure.error.reason}"
          )
          record.addReport(
            ProcessingFatal,
            s"Record deletion by id in Elasticsearch index $index failed: ${failure.error.reason}"
          )
      }
      .await
  }

  def close = {
    esClient.close()
  }
}

object MemobaseElasticClient {
  def apply(
      esApiKeyId: String,
      esApiKeySecret: String,
      esCaCertPath: String,
      esHosts: String
  ): Try[MemobaseElasticClient] = {
    val apiKeyAuth: String = Base64.getEncoder.encodeToString(
      s"$esApiKeyId:$esApiKeySecret".getBytes(StandardCharsets.UTF_8)
    )
    val headers: util.List[Header] = List(
      new BasicHeader("Authorization", s"ApiKey $apiKeyAuth")
        .asInstanceOf[Header]
    ).asJava
    val clientConfigCallback: HttpClientConfigCallback =
      (httpClientBuilder: HttpAsyncClientBuilder) => {
        httpClientBuilder
          .setSSLContext(TlsUtils.buildSSLContext(esCaCertPath).get)
          .setDefaultHeaders(headers)
      }
    Try(
      ElasticClient(
        JavaClient(
          ElasticProperties(esHosts),
          requestConfigCallback = NoOpRequestConfigCallback,
          httpClientConfigCallback = clientConfigCallback
        )
      )
    ).map(c => new MemobaseElasticClient(c))
  }
}
