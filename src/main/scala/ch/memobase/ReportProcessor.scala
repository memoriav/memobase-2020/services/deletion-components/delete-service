/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.logging.log4j.scala.Logging

class ReportProcessor(objectType: DeleteObject, val dryRun: Boolean)
    extends Logging {
  var reports: List[(ProcessingStatus, String)] = List()
  val id: String = objectType.id

  def addReport(status: ProcessingStatus, msg: String): Unit = reports =
    reports :+ ((status, msg))

  def sendReports(
      sessionId: String,
      reportStepId: String,
      reportingTopic: String,
      producer: KafkaProducer[String, String]
  ): Unit = {
    val recordSetId = (objectType: @annotation.nowarn(
      "msg=match may not be exhaustive"
    )) match {
      case Document(id)   => id.split('-').take(2).mkString("-")
      case RecordSet(id)  => id
      case Institution(_) => "-"
    }

    val institutionId = (objectType: @annotation.nowarn(
      "msg=match may not be exhaustive"
    )) match {
      case Document(id)    => id.split('-')(0)
      case RecordSet(id)   => id.split('-')(0)
      case Institution(id) => id
    }
    val recordHeaders =
      new RecordHeaders()
        .add("sessionId", sessionId.getBytes)
        .add("recordSetId", recordSetId.getBytes)
        .add("institutionId", institutionId.getBytes)
    val reportId: String = (objectType: @annotation.nowarn(
      "msg=match may not be exhaustive"
    )) match {
      case Document(i)    => s"https://memobase.ch/record/$i"
      case RecordSet(i)   => s"https://memobase.ch/recordSet/$i"
      case Institution(i) => s"https://memobase.ch/institution/$i"
    }
    for ((status, msg) <- reports) {
      val report = Report(reportId, status, msg, reportStepId)
      val idHashPostfix =
        s"$reportId${status.value}$msg$reportStepId$sessionId".hashCode
      logger.debug(
        s"Sending report for record $id with status ${status.value}: $msg"
      )
      // scalastyle:off
      producer.send(
        new ProducerRecord(
          reportingTopic,
          null,
          s"$reportId$idHashPostfix",
          report.toString,
          recordHeaders
        )
      )
      // scalastyle:on
    }
  }
}
