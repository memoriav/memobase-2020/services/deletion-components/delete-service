/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import org.apache.logging.log4j.scala.Logging

import scala.util.matching.Regex

object DeleteObject extends Logging {
  private val documentWithOriginalId: Regex =
    """^orig:([a-z]{3,5}-[0-9]{3}):(.*)$""".r
  private val institution: Regex = """^[a-z]{3,5}$|^[a-z]{2}-[0-9]{3}$""".r
  private val institutionFullyQualified: Regex =
    "^https://memobase.ch/institution/([a-z]{3,5})$|^[a-z]{2}-[0-9]{3}$".r
  private val recordSet: Regex = """^[a-z]{3,5}-\d{3}$""".r
  private val recordSetFullyQualified: Regex =
    """^https://memobase.ch/recordSet/([a-z]{3,5}-\d{3})$""".r
  private val document: Regex = """^[a-z]{3,5}-\d{3}-[\w-]+$""".r
  private val documentFullyQualified: Regex =
    """^https://memobase.ch/record/([a-z]{3,5}-\d{3}-[\w-]+)$""".r

  def mapToDeleteType(id: String): Option[DeleteObject] = id match {
    case documentWithOriginalId(recordSetId, documentId) =>
      logger.debug(
        s"Document with original ID $documentId in record set $recordSetId recognised"
      )
      Some(DocumentWithOriginalId(documentId, recordSetId))
    case institution() =>
      logger.debug(s"Institution ID $id recognised")
      Some(Institution(id))
    case institutionFullyQualified(i) =>
      logger.debug(s"Fully qualified institution ID $i recognised")
      Some(Institution(i))
    case recordSet() =>
      logger.debug(s"Record set ID $id recognised")
      Some(RecordSet(id))
    case recordSetFullyQualified(i) =>
      logger.debug(s"Fully qualified record set ID $i recognised")
      Some(RecordSet(i))
    case document() =>
      logger.debug(s"Record ID $id recognised")
      Some(Document(id))
    case documentFullyQualified(i) =>
      logger.debug(s"Fully qualified record ID $i recognised")
      Some(Document(i))
    case i =>
      logger.warn(s"Invalid ID! $i")
      None
  }
}

sealed trait DeleteObject {
  val name: String
  val id: String
  val fullyQualifiedId: String
}

case class Document(override val id: String) extends DeleteObject {
  override val name = "Document"
  override val fullyQualifiedId: String = s"https://memobase.ch/record/$id"
}

case class DocumentWithOriginalId(override val id: String, recordSetId: String)
    extends DeleteObject {
  override val name = "DocumentWithOriginalId"
  override val fullyQualifiedId: String = id
}

case class RecordSet(override val id: String) extends DeleteObject {
  override val name = "Recordset"
  override val fullyQualifiedId: String = s"https://memobase.ch/recordSet/$id"
}

case class Institution(override val id: String) extends DeleteObject {
  override val name = "Institution"
  override val fullyQualifiedId: String = s"https://memobase.ch/institution/$id"
}
