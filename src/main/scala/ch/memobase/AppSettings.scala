/*
 * Delete Service
 * Copyright (C) 2022  memoriav / Memobase 2020 / services / import-process
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.DeleteObject
import org.apache.logging.log4j.scala.Logging

import java.util.Properties

trait AppSettings {
  self: Logging =>

  val producerProps: Properties = {
    val props = new Properties()
    logger.debug("Kafka properties set: ")
    props.put("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    logger.debug(s"- bootstrap.servers: ${sys.env("KAFKA_BOOTSTRAP_SERVERS")}")
    props.put(
      "key.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    logger.debug(
      "- key.serializer: org.apache.kafka.common.serializiation.StringSerializer"
    )
    props.put(
      "value.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    logger.debug(
      "- value.serializer: org.apache.kafka.common.serializiation.StringSerializer"
    )
    props.put("batch.size", "131072")
    logger.debug("- batch.size: 131072")
    props.put("compression.type", "gzip")
    logger.debug("- compression.type: gzip")
    props.put("security.protocol", sys.env("SECURITY_PROTOCOL"))
    logger.debug(s"- security.protocol: ${sys.env("SECURITY_PROTOCOL")}")
    props.put("ssl.keystore.type", sys.env("SSL_KEYSTORE_TYPE"))
    logger.debug(s"- ssl.keystore.type: ${sys.env("SSL_KEYSTORE_TYPE")}")
    props.put("ssl.keystore.location", sys.env("SSL_KEYSTORE_LOCATION"))
    logger.debug(
      s"- ssl.keystore.location: ${sys.env("SSL_KEYSTORE_LOCATION")}"
    )
    props.put("ssl.keystore.password", sys.env("SSL_KEYSTORE_PASSWORD"))
    logger.debug(
      s"- ssl.keystore.password: ${sys.env("SSL_KEYSTORE_PASSWORD")}"
    )
    props.put("ssl.truststore.type", sys.env("SSL_TRUSTSTORE_TYPE"))
    logger.debug(s"- ssl.truststore.type: ${sys.env("SSL_TRUSTSTORE_TYPE")}")
    props.put("ssl.truststore.location", sys.env("SSL_TRUSTSTORE_LOCATION"))
    logger.debug(
      s"- ssl.truststore.type: ${sys.env("SSL_TRUSTSTORE_LOCATION")}"
    )
    props
  }

  val frontendDocumentsIndex: String =
    sys.env("ELASTIC_FRONTEND_DOCUMENTS_INDEX")
  logger.debug(s"Elasic frontend documents index: $frontendDocumentsIndex")
  val frontendRecordSetsIndex: String =
    sys.env("ELASTIC_FRONTEND_RECORD_SETS_INDEX")
  logger.debug(s"Elastic frontend record sets index: $frontendRecordSetsIndex")
  val frontendInstitutionsIndex: String =
    sys.env("ELASTIC_FRONTEND_INSTITUTIONS_INDEX")
  logger.debug(
    s"Elastic frontend institutions index: $frontendInstitutionsIndex"
  )
  val apiDocumentsIndex: String = sys.env("ELASTIC_API_DOCUMENTS_INDEX")
  logger.debug(s"Elastic API documents index: $apiDocumentsIndex")
  val apiRecordSetsIndex: String = sys.env("ELASTIC_API_RECORD_SETS_INDEX")
  logger.debug(s"Elastic API record sets index: $apiRecordSetsIndex")
  val apiInstitutionsIndex: String = sys.env("ELASTIC_API_INSTITUTIONS_INDEX")
  logger.debug(s"Elastic API institutions index: $apiInstitutionsIndex")

  val esHosts: String = sys
    .env("ELASTIC_HOST_LIST")
    .split(',')
    .toList
    .zipWithIndex
    .map(h =>
      if (h._2 == 0) {
        h._1
      } else {
        h._1.replaceAll("https?://", "")
      }
    )
    .mkString(",")
  logger.debug(s"Elastic hosts: $esHosts")
  val esApiKeyId: String = sys.env("ELASTIC_API_KEY_ID")
  val esApiKeySecret: String = sys.env("ELASTIC_API_KEY_SECRET")
  logger.debug("Got Elastic API key id and secret")
  val esCaCertPath: String = sys.env("CA_CERT_PATH")
  logger.debug(s"Path to CA certificate: $esCaCertPath")

  val reportStepId: String = "00-delete-service"
  logger.debug(s"Report step id: $reportStepId")
  val reportingTopic: String = sys.env("TOPIC_PROCESS")
  logger.debug(s"Reporting topic: $reportingTopic")
  val jsonldDocumentsTopic: String = sys.env("TOPIC_JSONLD_DOCUMENTS")
  logger.debug(s"JsonLD documents topic: $jsonldDocumentsTopic")

  val mariaDBDSN: String =
    s"jdbc:mariadb://${sys.env("MARIADB_HOST")}:${sys.env("MARIADB_PORT")}/${sys
      .env("MARIADB_DATABASE")}?sslMode=verify-full&serverSslCert=${sys.env("MARIADB_HOST_CERTS")}"
  val mariaDBPassword: String =
    sys.env("MARIADB_PASSWORD").replaceAll("\\s+$", "")
  logger.debug("MariaDB password: ***")
  val mariaDBTables: List[String] = sys
    .env("MARIADB_TABLES")
    .split(", *")
    .toList
  logger.debug(s"MariaDB tables: ${sys.env("MARIADB_TABLES")}")
  val mariaDBUser: String = sys.env("MARIADB_USER").replaceAll("\\s+$", "")
  logger.debug(s"MariaDB user: $mariaDBUser")

  val postgresDBUser: String = sys.env("POSTGRES_USER").replaceAll("\\s+$", "")
  logger.debug(s"Postgres user: $postgresDBUser")
  val postgresDBPassword: String =
    sys.env("POSTGRES_PASSWORD").replaceAll("\\s+$", "")
  logger.debug("Postgres password: ***")
  val postgresDBHost: String =
    s"jdbc:postgresql://${sys.env("POSTGRES_HOST")}:${sys
      .env("POSTGRES_PORT")}/${sys.env("POSTGRES_DATABASE")}"
  logger.debug(s"Postgres host: $postgresDBHost")

  val ids: List[DeleteObject] = sys
    .env("IDS")
    .split(";")
    .toList
    .flatMap(id => DeleteObject.mapToDeleteType(id))
  logger.debug(s"IDs to be processed: ${sys.env("IDS")}")
  val dryRun: Boolean = sys.env.get("DRY_RUN").contains("1")
  logger.debug(s"Dry run: ${dryRun.toString}")
  val recursiveDelete: Boolean = sys.env.get("RECURSIVE_DELETE").contains("1")
  logger.debug(s"Recursive delete: ${recursiveDelete.toString}")
  val sessionId: String = sys.env("SESSION_ID")
  logger.debug(s"Session id: $sessionId")

  val mediaFolderRootPath: String = sys.env("MEDIA_FOLDER_ROOT_PATH")
  logger.debug(s"Media folder root path: $mediaFolderRootPath")
  val mediaFolderCacheRootPath: String = sys.env("MEDIA_FOLDER_CACHE_ROOT_PATH")
  logger.debug(s"Media folder cache root path: $mediaFolderCacheRootPath")
}
