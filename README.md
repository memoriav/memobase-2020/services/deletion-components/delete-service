# Delete Service

Removes documents from Memobase indices

This service deletes documents based on document, record set or institution id in the different Memobase indices and
data folders, namely in the

- frontend index,
- OAI-PMH index,
- API index and
- media server index
- media data folders (dissemination copies and cached thumbnails)

Furthermore, it marks the respective documents in the Memobase "Single Source of Truth" as `DELETED`.

The service is called manually (normally via the Import API) or by a service which integrates data via a remote API (
e.g. SRG API crawler.). Therefore, it is deployed as a short-running job on Kubernetes.

## Configuration

The application expects a couple of settings provided by environment variables (as these are expected to be shared among
the jobs):

### General settings

- `IDS`: Semicolon-separated list of ids of records, record sets or institutions to
  be deleted. If a record id is prefixed with `orig:`, the id is treated as original id (i.e. the id used by the data
  provider). In this case, you further have to provide a record set id (in order to unequivocally identify a matching
  document). The entire string looks like this: `orig:{recordSetId}:{documentId}`, e.g. `orig:abc-001:document01`.
- `SESSION_ID`: Unique id for running the job
- `RECURSIVE_DELETE`: Delete institution and record set objects as well
- `DRY_RUN`: Do a dry-run 
- `CA_CERT_PATH`: Path to CA certificate

### Kafka

- `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap servers
- `TOPIC_PROCESS`: Topics where reports are written to
- `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
- `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
- `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
- `SSL_KEYSTORE_PASSWORD`: The password of keystore
- `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
- `SSL_TRUSTSTORE_LOCATION: The location of the trust store file

### ElasticSearch

- `ELASTIC_API_KEY_ID`: Elasticsearch API key id
- `ELASTIC_API_KEY_SECRET`: Elasticsearch API key secret
- `ELASTIC_FRONTEND_DOCUMENTS_INDEX`: Name of index where the document data as used in the frontend is written to
- `ELASTIC_FRONTEND_RECORD_SETS_INDEX`: Name of index where the record set data as used in the frontend is written to
- `ELASTIC_FRONTEND_INSTITUTIONS_INDEX`: Name of index where the institution data as used in the frontend is written to
- `ELASTIC_API_DOCUMENTS_INDEX`: Name of index where the document data as used in the API is written to
- `ELASTIC_API_RECORD_SETS_INDEX`: Name of index where the record set data as used in the API is written to
- `ELASTIC_API_INSTITUTIONS_INDEX`: Name of index where the institution data as used in the API is written to
- `ELASTIC_HOST`: Hostname of Elasticsearch instance
- `ELASTIC_PORT`: Port of Elasticsearch instance

### MariaDB
- `MARIADB_HOST`: Hostname of MariaDB instance
- `MARIADB_PORT`: Port of MariaDB instance
- `MARIADB_DATABASE`: MariaDB database name
- `MARIADB_USER`: MariaDB database user
- `MARIADB_PASSWORD`: MariaDB user password
- `MARIADB_TABLES`: Relevant MariaDB tables
- `MARIADB_HOST_CERTS`: Path to MariaDB ca certificates

### PostgreSQL

- `POSTGRES_HOST`: Hostname of PostgreSQL instance
- `POSTGRES_PORT`: Port of PostgreSQL instance
- `POSTGRES_DATABASE`: PostgreSQL database name
- `POSTGRES_USER`: PostgreSQL database user
- `POSTGRES_PASSWORD`: PostgreSQL user password
